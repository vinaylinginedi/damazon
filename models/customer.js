/**
 * Customer Scheme to store customer details
 * @author Anuraag Naidu Sanakkayala
 */const mongoose = require('mongoose')      

const CustomerSchema = new mongoose.Schema({

  _id: { type: Number, required: true, default: 12345 },
  firstname: {
    type: String,
    required: true,
    default: 'First Name'
  },
  
  lastname: {
    type: String,
    required: true,
    default: 'Last Name'
  },
  age: {
    type: Number,
    required: false,
    default: 18
  },
  phonenumber: {
      type: Number,
      required: true,
      default: 00000000000
  },
  address: {
      type: String,
      required: true,
      default: 'address'
  }
})
module.exports = mongoose.model('Customer', CustomerSchema)
